# Course List Services Project

### Link to Gitlab Repository
https://gitlab.com/420assignments/project

### Link to Deployment Server
http://10.172.24.245:8000/

## List of Group Members
1. Michael Derocher 2135832
2. Sila Ben-Khelifa 2135666
3. Paul Alexandre Muraru 2143549


## Development Setup Steps
1. Ensure Python 3.7 or greater is installed
2. Create a virtual environment on your machine (e.g., python -m venv .venv)
3. Activate your virtual environment
4. Install all the requirements in requirements.txt (e.g., pip install -r requirements.txt)
5. Setup the database credentials by setting the environment variables DBUSER and DBPWD
6. Ensure that you are connected to the Dawson network in order to access the database (e.g., local connection, VPN)
7. Initialize the database with the setup file located at ./CourseListServices/DB/sql/setup.sql
8. Run the debug server (flask --app CourseListServices --debug run)

## Deployment Setup Step
1. Ensure Python 3.7 or greater is installed
2. Create a virtual environment on your machine (e.g., python -m venv .venv)
3. Activate your virtual environment
4. Install all the requirements in requirements.txt (e.g., pip install -r requirements.txt)
5. Setup the database credentials by setting the environment variables DBUSER and DBPWD
6. Ensure that you are connected to the Dawson network in order to access the database (e.g., local connection, VPN)
7. Initialize the database with the setup file located at ./CourseListServices/DB/sql/setup.sql
8. Run the server (gunicorn -b 0.0.0.0:8000 'CourseListServices:create_app()')


## Usage
There are only 2 different passwords for the users :
Python420 for the instructors
and
dawson1234 for the student accounts

