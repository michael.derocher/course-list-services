from flask import Blueprint, flash, render_template, request, redirect, url_for
from flask_login import current_user, login_required
from werkzeug.security import check_password_hash, generate_password_hash
import flask_login

from CourseListServices.UserManagement.member import DeleteUserForm, EditGroupForm
from ..DB.dbmanager import get_db

bp = Blueprint("groups", __name__, url_prefix='/admin/')

@bp.route('/groups/')
@login_required
def display_all_groups():
    try:
        database = get_db()
    except Exception as e:
        flash('Unable to connect with the database')
    
    groups=database.get_groups()
    return render_template('groups.html', groups=groups)

@bp.route('/groups/<groupname>/', methods=['GET', 'POST'])
@login_required
def display_group(groupname):
    try:
        database = get_db()
    except Exception as e:
        flash('Unable to connect with the database')
    
    group=database.get_group(groupname)
    members=database.get_group_members(groupname)

    form = EditGroupForm()
    # this is done this way so that the current group is the default option in the choices
    form.groupname.choices = [groupname]
    # this gets the other groups that the user can modify
    form.groupname.choices += [(g.groupname) for g in database.get_groups() if g.groupname != 'root' and current_user.group.can_modify_other_user(g) and g.groupname != groupname]

    form2 = DeleteUserForm()

    if form.validate_on_submit():
        group = database.get_group(form.groupname.data)
        email = form.email.data

        if group:
            database.add_group_member(group.groupname, email)
            return redirect(url_for('groups.display_group', groupname=group.groupname, members=members, form=form, form2=form2, user=current_user))
        
        return render_template('group.html', group=group, members=members, form=form, form2=form2, user=current_user)
    if form2.validate_on_submit():
        email = form2.email.data
        database.delete_user(email)
        return redirect(url_for('groups.display_group', groupname=group.groupname, members=members, form=form, form2=form2, user=current_user))
    return render_template('group.html', group=group, members=members, form=form, form2=form2, user=current_user)
