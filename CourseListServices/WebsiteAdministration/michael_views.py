from flask import Blueprint, send_file

bp = Blueprint("michael", __name__, url_prefix='/michael/')

@bp.route('/michael/')
def michael_sfx():
    return send_file('static/sfx/michael_dont_leave_me_here.mp3')