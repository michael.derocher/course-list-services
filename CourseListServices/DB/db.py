import oracledb
import os
from CourseListServices.CCDisplay.course import Course
from CourseListServices.CCDisplay.competency import Competency
from CourseListServices.CCDisplay.element import Element
from CourseListServices.UserManagement.member import User
from CourseListServices.UserManagement.group import Group
from CourseListServices.CCDisplay.term import Term
from CourseListServices.CCDisplay.domain import Domain

class Database:
    def __init__(self, autocommit=True):
        self.__conn = self.__connect()
        self.__conn.autocommit = autocommit

    def run_file(self, file_path):
        statement_parts = []
        with self.__conn.cursor() as cursor:
            with open(file_path, 'r') as f:
                for line in f:
                    statement_parts.append(line)
                    if line.strip('\n').strip('\n\r').strip().endswith(';'):
                        statement = "".join(
                            statement_parts).strip().rstrip(';')
                        if statement:
                            try:
                                cursor.execute(statement)
                            except Exception as e:
                                print(e)
                        statement_parts = []

    def close(self):
        '''Closes the connection'''
        if self.__conn is not None:
            self.__conn.close()
            self.__conn = None

    #Inserts the user to the DB
    def insert_user(self, user):
        if not isinstance(user, User):
            raise TypeError()   
        # Insert the post to the DB
        with self.__conn.cursor() as cursor:
            cursor.execute('insert into cc_service_users (email, password, name, groupname) values (:email, :password, :name, :groupname)',
                           email=user.email, password=user.password, name=user.name, groupname=user.group.groupname)

    #Gets a user with an email from database      
    def get_user(self, email):
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select email, password, id, name, groupname from cc_service_users where email=:email', email=email)
            for row in results:
                group = self.get_group(row[4])
                user = User(row[0], row[1], row[3], group=group)
                user.id = row[2]
                return user
            
    def delete_user(self, email):
        with self.__conn.cursor() as cursor:
            cursor.execute('delete from cc_service_users where email=:email', email=email)
    
    #Gets a user from database using an id
    def get_user_id(self, id):
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select email, password, id, name, groupname from cc_service_users where id=:id', id=id)
            for row in results:
                group = self.get_group(row[4])
                user = User(row[0], row[1], row[3], group=group)
                user.id = row[2]
                return user
                
    # given a groupname (or the primary key of the object), return the group object
    def get_group(self, groupname):
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select groupname, permissions from cc_service_groups where groupname=:groupname', groupname=groupname)
            for row in results:
                group = Group(row[0], row[1])
                return group

    # just gets all the group objects
    def get_groups(self):
        groups = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select groupname, permissions from cc_service_groups')
            for row in results:
                group = Group(row[0], row[1])
                groups.append(group)
        return groups
    
    # given a groupname, it gets all the users that returns a list of users
    def get_group_members(self, groupname):
        members = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select email, password, id, name, groupname from cc_service_users where groupname=:groupname', groupname=groupname)
            for row in results:
                group = self.get_group(row[4])
                user = User(row[0], row[1], row[3], group=group)
                user.id = row[2]
                members.append(user)
                
        return members
    
    # modifies a user to change their group
    def add_group_member(self, groupname, email):
        with self.__conn.cursor() as cursor:
            self.remove_group_member(email)
            cursor.execute('update cc_service_users set groupname=:groupname where email=:email', groupname=groupname, email=email)

    # removes a user from a group and puts them back in member
    def remove_group_member(self, email):
        with self.__conn.cursor() as cursor:
            cursor.execute('update cc_service_users set groupname=:groupname where email=:email', groupname='member', email=email)

    #Changes the password
    def update_password(self, id, password):
        with self.__conn.cursor() as cursor:
            cursor.execute('update cc_service_users set password=:password where id=:id', password=password, id=id)

    #Changes the name
    def update_name(self, id, name):
        with self.__conn.cursor() as cursor:
            cursor.execute('update cc_service_users set name=:name where id=:id', name=name, id=id)
            
    def get_courses(self):
        courses = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id from courses order by term_id')
            for row in results:
                domain = self.get_domain(row[6])
                term = self.get_term(row[7])
                course_hours = self.get_course_hours(row[0])
                course_credits = float(self.get_course_credits(row[0]))
                course = Course(row[0], row[1], row[2], row[3], row[4], course_hours, course_credits, row[5], domain, term)
                courses.append(course)
        return courses
    
    def get_specific_course(self, course_id):
        with self.__conn.cursor() as cursor:
            result = cursor.execute('select course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id from courses where course_id=:course_id', course_id=course_id)
            for row in result:
                domain = self.get_domain(row[6])
                term = self.get_term(row[7])
                course_hours = self.get_course_hours(row[0])
                course_credits = float(self.get_course_credits(row[0]))
                return Course(row[0], row[1], row[2], row[3], row[4], course_hours, course_credits, row[5], domain, term)
    
    def get_domain(self, domain_id):
        with self.__conn.cursor() as cursor:
            result = cursor.execute('select domain_id, domain, domain_description from domains where domain_id=:domain_id', domain_id=domain_id)
            for row in result:
                return Domain(row[0], row[1], row[2])
    
    def get_term(self, term_id):
        with self.__conn.cursor() as cursor:
            result = cursor.execute('select term_id, term_name from terms where term_id=:term_id', term_id=term_id)
            for row in result:
                return Term(row[0], row[1])

    
    def get_course_competencies(self, course_id):
        competencies = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select unique competency_id, competency, competency_achievement, competency_type from view_courses_elements_competencies where course_id=:course_id order by competency_id', course_id=course_id)
            for row in results:
                competency = Competency(row[0], row[1], row[2], row[3])
                competencies.append(competency)
        return competencies
    
    def get_course_elements(self, course_id):
        elements = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select unique element_id, element_order, element, element_criteria, competency_id from view_courses_elements where course_id=:course_id order by competency_id, element_order', course_id=course_id)
            for row in results:
                element = Element(row[0], row[1], row[2], row[3], row[4])
                hours = self.get_course_element_hours(course_id, element.element_id)
                element.set_elem_hours(hours)
                elements.append(element)
        return elements
    
    def get_course_element_hours(self, course_id, element_id):
        with self.__conn.cursor() as cursor:
            result = cursor.execute('select element_hours from courses_elements where course_id=:course_id and element_id=:element_id',
                           course_id=course_id,
                           element_id=element_id)
            for row in result:
                return row[0]
            
    
    def get_course_hours(self, course_id):
        with self.__conn.cursor() as cursor:
            result = cursor.callfunc("courses_package.calculate_total_hours", int, [course_id])
            return result
    
    def get_course_credits(self, course_id):
        with self.__conn.cursor() as cursor:
            result = cursor.callfunc("courses_package.calculate_credits", int, [course_id])
            return result
        
    def add_course(self, newCourse):
        with self.__conn.cursor() as cursor:
            cursor.execute('insert into courses values(:course_id, :course_title, :theory_hours, :lab_hours, :work_hours, :course_description, :domain_id, :term_id)',
                           course_id=newCourse.course_id,
                           course_title=newCourse.course_name,
                           theory_hours=newCourse.theory_hours,
                           lab_hours=newCourse.lab_hours,
                           work_hours=newCourse.work_hours,
                           course_description=newCourse.course_description,
                           domain_id=newCourse.domain.domain_id,
                           term_id=newCourse.term.term_id)
        
    def edit_course(self, updatedCourse):
        with self.__conn.cursor() as cursor:
            cursor.callproc('courses_package.update_course', 
                            [updatedCourse.course_id, 
                             updatedCourse.course_name, 
                             updatedCourse.theory_hours, 
                             updatedCourse.lab_hours, 
                             updatedCourse.work_hours, 
                             updatedCourse.course_description, 
                             updatedCourse.domain.domain_id, 
                             updatedCourse.term.term_id])
    
    def delete_course(self, course_id):
        with self.__conn.cursor() as cursor:
            cursor.callproc('courses_package.delete_course', [course_id])

    def add_course_element(self, course_id, element_id, hours):
        with self.__conn.cursor() as cursor:
            cursor.execute('insert into courses_elements values (:course_id, :element_id, :hours)',
                           course_id=course_id,
                           element_id=element_id,
                           hours=hours)
            
    def edit_course_element(self, course_id, element_id, hours):
        with self.__conn.cursor() as cursor:
            cursor.execute('UPDATE courses_elements SET element_hours=:hours WHERE course_id=:course_id and element_id=:element_id',
                           course_id=course_id,
                           element_id=element_id,
                           hours=hours)
            
    def delete_course_element(self, course_id, element_id):
        with self.__conn.cursor() as cursor:
            cursor.execute('delete from courses_elements where course_id=:course_id and element_id=:element_id',
                           course_id=course_id,
                           element_id=element_id)
        
    def get_competencies(self):
        competencies = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select competency_id, competency, competency_achievement, competency_type from competencies order by competency_id')
            for row in results:
                comp = Competency(row[0], row[1], row[2], row[3])
                competencies.append(comp)
        return competencies
    
    def get_specific_competency(self, competency_id):
        with self.__conn.cursor() as cursor:
            result = cursor.execute('select competency_id, competency, competency_achievement, competency_type from competencies where competency_id=:competency_id', competency_id=competency_id)
            for row in result:
                return Competency(row[0], row[1], row[2], row[3])
            
    def get_competency_elements(self, competency_id):
        elements = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select element_id, element_order, element, element_criteria, competency_id from elements where competency_id=:competency_id order by element_order', competency_id=competency_id)
            for row in results:
                elements.append(Element(row[0], row[1], row[2], row[3], row[4]))
        return elements
    
    def add_competency(self, newCompetency):
        with self.__conn.cursor() as cursor:
            cursor.execute('insert into competencies values(:compId,:compName,:compAchievement,:compType)', compId=newCompetency.competency_id, compName=newCompetency.competency, compAchievement=newCompetency.competency_achievement, compType=newCompetency.competency_type)
                
    def edit_competency(self, updatedCompetency):
        with self.__conn.cursor() as cursor:
            cursor.callproc('courses_package.update_competency', 
                            [updatedCompetency.competency_id, 
                             updatedCompetency.competency, 
                             updatedCompetency.competency_achievement, 
                             updatedCompetency.competency_type])
    
    def delete_competency(self, competency_id):
        with self.__conn.cursor() as cursor:
            compElements = self.get_competency_elements(competency_id)
            for elem in compElements:
                cursor.callproc('courses_package.delete_element', [elem.element_id])
            cursor.callproc('courses_package.delete_competency', [competency_id])
    
    def get_relevant_courses(self, competency_id):
        courses = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select unique course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id from view_courses_elements_competencies where competency_id=:competency_id', competency_id=competency_id)
            for row in results:
                domain = self.get_domain(row[6])
                term = self.get_term(row[7])
                course_hours = self.get_course_hours(row[0])
                course_credits = float(self.get_course_credits(row[0]))
                course = Course(row[0], row[1], row[2], row[3], row[4], course_hours, course_credits, row[5], domain, term)
                courses.append(course)
            return courses
        
    def get_element(self, element_id):
        with self.__conn.cursor() as cursor:
            result = cursor.execute('select element_id, element_order, element, element_criteria, competency_id from elements where element_id=:element_id',element_id=element_id)
            for row in result:
                return Element(row[0], row[1], row[2], row[3], row[4])
            
    def add_element(self, newElement):
        with self.__conn.cursor() as cursor:
            cursor.execute('insert into elements (element_order, element, element_criteria, competency_id) values(:element_order, :element, :element_criteria, :competency_id)',
                           element_order=newElement.element_order,
                           element=newElement.element,
                           element_criteria=newElement.element_criteria,
                           competency_id=newElement.competency_id)
            
    def edit_element(self, updatedElement):
        with self.__conn.cursor() as cursor:
            cursor.execute('UPDATE elements SET element_order=:element_order, element=:element, element_criteria=:element_criteria WHERE element_id=:element_id',
                           element_id=updatedElement.element_id,
                           element_order=updatedElement.element_order,
                           element=updatedElement.element,
                           element_criteria=updatedElement.element_criteria)
            
    def delete_element(self, element_id):
        with self.__conn.cursor() as cursor:
            cursor.callproc('courses_package.delete_element', [element_id])
        
    def get_domains(self):
        domains = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select domain_id, domain, domain_description from domains order by domain_id')
            for row in results:
                domain = Domain(row[0], row[1], row[2])
                domains.append(domain)
            return domains
        
    def get_specific_domain(self, domain_id):
        with self.__conn.cursor() as cursor:
            result = cursor.execute('select domain_id, domain, domain_description from domains where domain_id=:domain_id', domain_id=domain_id)
            for row in result:
                return Domain(row[0], row[1], row[2])
            
    def get_domain_courses(self, domain_id):
        courses = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select unique course_id, course_title, theory_hours, lab_hours, work_hours, description, domain_id, term_id from courses where domain_id=:domain_id', domain_id=domain_id)
            for row in results:
                domain = self.get_domain(row[6])
                term = self.get_term(row[7])
                course_hours = self.get_course_hours(row[0])
                course_credits = float(self.get_course_credits(row[0]))
                course = Course(row[0], row[1], row[2], row[3], row[4], course_hours, course_credits, row[5], domain, term)
                courses.append(course)
            return courses
        
    def add_domain(self, newDomain):
        with self.__conn.cursor() as cursor:
            cursor.execute('insert into domains values(:domain_id, :domain, :domain_description)',
                           domain_id=newDomain.domain_id,
                           domain=newDomain.domain,
                           domain_description=newDomain.domain_description)
        
    def edit_domain(self, updatedDomain):
        with self.__conn.cursor() as cursor:
            cursor.execute('update domains SET domain=:domain, domain_description=:domain_description WHERE domain_id=:domain_id',
                           domain_id=updatedDomain.domain_id,
                           domain=updatedDomain.domain,
                           domain_description=updatedDomain.domain_description)
            
    def delete_domain(self, domain_id):
        with self.__conn.cursor() as cursor:
            courses = self.get_domain_courses(domain_id)
            for course in courses:
                cursor.callproc('courses_package.delete_course', [course.course_id])
            cursor.callproc('courses_package.delete_domain', [domain_id])
        
    def get_terms(self):
        terms = []
        with self.__conn.cursor() as cursor:
            results = cursor.execute('select term_id, term_name from terms order by term_id')
            for row in results:
                term = Term(row[0], row[1])
                terms.append(term)
            return terms
    
    #Gets all members from db
    def get_users(self):
        users = []
        with self.__conn.cursor() as cursor:
            result = cursor.execute("select email, password, id, name, groupname from cc_service_users")
            for row in result:
                user = User(row[0], row[1], row[3])
                user.id = row[2]
                user.group = self.get_group(row[4])
                users.append(user)
        return users

    def __get_cursor(self):
            for i in range(3):
                try:
                    return self.__conn.cursor()
                except Exception as e:
                    # Might need to reconnect
                    self.__reconnect()

    def __reconnect(self):
        try:
            self.close()
        except oracledb.Error as f:
            pass
        self.__conn = self.__connect()

    def __connect(self):
        return oracledb.connect(user=os.environ['DBUSER'], password=os.environ['DBPWD'],
                                             host="198.168.52.211", port=1521, service_name="pdbora19c.dawsoncollege.qc.ca")

#To allow the running of the file to initialize database with init-db command
if __name__ == '__main__':
    print('Provide file to initialize database')
    file_path = input()
    if os.path.exists(file_path):
        db = Database()
        db.run_file(file_path)
        db.close()
    else:
        print('Invalid Path')
