import click
import os
from flask import current_app, g
from .db import Database

#Gets database connection
def get_db():
    if 'db' not in g:
        g.db = Database()
    return g.db

#Closes database connection
def close_db():
    if 'db' in g:
        db = g.pop('db', None)
    if 'db' != None:
        db.close()

#To initialize database through terminal
def init_db():
    database = get_db()
    path = os.path.join(current_app.root_path, 'DB')
    path = os.path.join(path, 'sql')
    database.run_file(os.path.join(path,'setup.sql'))

#To be able to use custom flask command 
@click.command('init-db')
def init_db_command():
    init_db()