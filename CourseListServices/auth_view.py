from flask import Blueprint, flash, render_template, request, redirect, url_for, current_app, send_from_directory
from werkzeug.security import check_password_hash, generate_password_hash
from .DB.dbmanager import get_db
from .UserManagement.member import SignupForm, User, LoginForm
from flask_login import login_user, logout_user, login_required, current_user
import os

bp = Blueprint("auth", __name__, url_prefix='/auth/')

#Signup for user
@bp.route('/signup/', methods=['GET', 'POST'])
def signup():
    #Check to see if user is already logged in
    if current_user.is_authenticated:
        flash('You are already logged in, logout to access the page')
        return redirect(url_for('courseComp.display_courses'))
    
    form = SignupForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            #To add an avatar
            file = form.avatar.data
            avatar_dir = os.path.join(current_app.config['IMAGE_PATH'], form.email.data)
            if not os.path.exists(avatar_dir):
                os.makedirs(avatar_dir)
            avatar_path = os.path.join(avatar_dir, 'avatar.png')
            file.save(avatar_path)

            #Password hashing
            hash = generate_password_hash(form.password.data)
            user = User(form.email.data, hash, form.name.data)
            try:
                get_db().insert_user(user)
            except:
                flash("User already exists")
                return render_template('signup.html', form=form)
            flash("User added")
            return redirect(url_for('courseComp.display_courses'))
    return render_template('signup.html', form=form)

#Login for user
@bp.route('/login/', methods=['GET', 'POST'])
def login():
    #Check to see if user is already logged in
    if current_user.is_authenticated:
        flash('You are already logged in, logout to access the page')
        return redirect(url_for('courseComp.display_courses'))
    
    form = LoginForm()
    #Handle logging in
    if request.method == 'POST':
        if form.validate_on_submit():
            email = form.email.data
            user = get_db().get_user(email)
            if user:
                if user.group.has_permission('cannotLogin'):
                    flash('User cannot login')
                    return redirect(url_for('auth.login'))
                #Check password
                pwd = form.password.data
                if check_password_hash(user.password, pwd):
                    login_user(user, remember=form.remember_me.data)
                    flash("Logged in!")
                    return redirect(url_for('courseComp.display_courses'))
                else:
                    flash("Invalid information")
            else:
                flash('Could not find user')
                redirect(url_for('auth.signup'))
        else:
            flash("Invalid form")
    return render_template('login.html', form=form)

#Logout the user
@bp.route('/logout/')
@login_required
def logout():
    logout_user()
    return render_template('logout.html')

#Route to get the avatar of each user when loading it up
@bp.route('/avatar/<email>/avatar.png')
def get_avatar(email):
    avatar_dir = os.path.join(current_app.config['IMAGE_PATH'], email)
    return send_from_directory(avatar_dir, 'avatar.png')
