from flask import Flask, render_template
from flask_login import LoginManager
import secrets
import os

from .DB.dbmanager import get_db, init_db_command, close_db


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=secrets.token_urlsafe(32),
        IMAGE_PATH=os.path.join(app.instance_path, 'images')
    )
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    #Custom 404 page
    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('page_not_found.html'), 404

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    #To be able to user flask user
    @login_manager.user_loader
    def load_user(user_id):
        return get_db().get_user_id(int(user_id))
    
    app.teardown_appcontext(cleanup)

    init_app(app)

    return app


def init_app(app):
    #set up database and register blueprints
    app.cli.add_command(init_db_command)

    from .auth_view import bp as auth_bp
    app.register_blueprint(auth_bp)
    
    from .CCDisplay.course_comp_api import bp as course_comp_api_bp
    app.register_blueprint(course_comp_api_bp)

    from .UserManagement.members_views import bp as admin_bp
    app.register_blueprint(admin_bp)
    
    from .CCDisplay.cc_views import bp as course_comp_bp
    app.register_blueprint(course_comp_bp)
    
    from .WebsiteAdministration.michael_views import bp as michael_bp
    app.register_blueprint(michael_bp) 

    from .WebsiteAdministration.group_views import bp as groups_bp
    app.register_blueprint(groups_bp)


def cleanup(value):
    close_db
