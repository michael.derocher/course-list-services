import unittest
import requests

class CourseTests(unittest.TestCase):
    
    def test_get_course(self):
        url = "http://127.0.0.1:5000/api/courses/?course_id=420-110-DW"
        resp = requests.get(url)

        json_text = resp.json()

        self.assertEqual(200, resp.status_code)
        self.assertEqual("420-110-DW", json_text["course_id"])
        self.assertEqual("Programming I", json_text["course_name"])

    def test_post_course(self):
        json = {
            "course_id":"620-110-DW",
            "course_name":"Programming 6",
            "theory_hours":3,
            "lab_hours":3,
            "work_hours":3,
            "course_description":"This is a new course",
            "domain_id":1,
            "domain":"domain",
            "domain_description":"This is a domain",
            "term_id":1,
            "term_name":"fall"
        }
        url = "http://127.0.0.1:5000/api/courses/"
        resp = requests.post(url, json=json)

        self.assertEqual(200, resp.status_code)

    def test_edit_course(self):
        json = {
            "course_id":"620-110-DW",
            "course_name":"Programminggg 6",
            "theory_hours":3,
            "lab_hours":3,
            "work_hours":3,
            "course_description":"This is an edited course",
            "domain_id":1,
            "domain":"domain",
            "domain_description":"This is a domain",
            "term_id":1,
            "term_name":"fall"
        }
        url = "http://127.0.0.1:5000/api/courses/"
        resp = requests.put(url, json=json)

        self.assertEqual(201, resp.status_code)

    def test_delete_course(self):
        url = "http://127.0.0.1:5000/api/courses/?course_id=620-110-DW"
        resp = requests.delete(url)

        self.assertEqual(200, resp.status_code)