import unittest
import requests

class ElementTests(unittest.TestCase):

    def test_get_element(self):
        url = "http://127.0.0.1:5000/api/courses/420-110-DW/elements/?element_id=1"
        resp = requests.get(url)

        json_text = resp.json()

        self.assertEqual(200, resp.status_code)
        self.assertEqual(1, json_text["element_id"])
        self.assertEqual("Analyze the problem.", json_text["element"])

    def test_post_element(self):
        json = {
            "element_order":5,
            "element":"new element",
            "element_criteria":"This is a new element",
            "competency_id":"00Q2"
        }
        url = "http://127.0.0.1:5000/api/courses/420-110-DW/elements/"
        resp = requests.post(url, json=json)

        self.assertEqual(200, resp.status_code)

    def test_edit_element(self):
        json = {
            "element_id":2,
            "element_order":5,
            "element":"new element",
            "element_criteria":"This is a new element",
            "competency_id":"00Q2"
        }
        url = "http://127.0.0.1:5000/api/courses/420-110-DW/elements/"
        resp = requests.put(url, json=json)

        self.assertEqual(201, resp.status_code)

    def test_delete_element(self):
        url = "http://127.0.0.1:5000/api/courses/420-110-DW/elements/?element_id=1"
        resp = requests.delete(url)

        self.assertEqual(200, resp.status_code)
