import unittest
import requests

class CompetencyTests(unittest.TestCase):

    def test_get_competency(self):
        url = "http://127.0.0.1:5000/api/courses/420-110-DW/elements/competencies/?competency_id=00Q2"
        resp = requests.get(url)

        json_text = resp.json()

        self.assertEqual(200, resp.status_code)
        self.assertEqual("00Q2", json_text["competency_id"])
        self.assertEqual("Use programming languages", json_text["competency"])

    def test_post_competency(self):
        json = {
            "competency_id":"00M1",
            "competency":"New competency",
            "competency_achievement":"This is a new competency",
            "competency_type":"mandatory"
        }
        url = "http://127.0.0.1:5000/api/courses/420-110-DW/elements/competencies/"
        resp = requests.post(url, json=json)

        self.assertEqual(200, resp.status_code)

    def test_delete_competency(self):
        url = "http://127.0.0.1:5000/api/courses/420-110-DW/elements/competencies/?competency_id=00M1"
        resp = requests.delete(url)

        self.assertEqual(200, resp.status_code)