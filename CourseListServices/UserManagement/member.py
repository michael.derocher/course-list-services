from flask_login import UserMixin
from .group import Group
class User(UserMixin):
    # Group("member","") is the default user with no permissions
    def __init__(self, email, password, name, group=Group("member","")):
        if not isinstance(email, str):
            raise TypeError()
        if not isinstance(password, str):
            raise TypeError()
        if not isinstance(name, str):
            raise TypeError()
        if not isinstance(group, Group):
            raise TypeError()
        self.email = email
        self.name=name
        self.password = password
        self.id = None
        self.group = group
    
    def __repr__(self):
        return f'{self.id}: {self.name}, {self.email}, {self.group.groupname}'
    
    def __str__(self):
        return_string = f'{self.id}: {self.name}, {self.email}'

        if str(self.group):
            return_string += f", {self.group.groupname}"

        return return_string

from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import EmailField, PasswordField, StringField, BooleanField, FormField, TextAreaField, SubmitField, SelectField, HiddenField
from wtforms.validators import DataRequired, Length
class SignupForm(FlaskForm):
    email = EmailField("email", validators=[DataRequired()])
    password = PasswordField("password", validators=[DataRequired()])
    name = StringField("name", validators=[DataRequired()])
    avatar = FileField("avatar", validators=[DataRequired()])
    submit = SubmitField('Submit')

class LoginForm(FlaskForm):
    email = EmailField("email", validators=[DataRequired()])
    password = PasswordField("password", validators=[DataRequired()])
    remember_me = BooleanField("remember_me")
    submit = SubmitField('Submit')

class ResetPasswordForm(FlaskForm):
    old_password = PasswordField("old password", validators=[DataRequired()])
    new_password = PasswordField("new password", validators=[DataRequired()])
    submit = SubmitField('Submit')

class EditProfileForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    avatar = FileField("Avatar")
    password = PasswordField("New password")
    submit = SubmitField('Submit')

# hiddenfield is used to get back data from the form.
# this form is used to modify a users group
class EditGroupForm(FlaskForm):
    groupname = SelectField('Groupname', validators=[DataRequired()])
    email = HiddenField('Email', validators=[DataRequired()])
    submit = SubmitField('Submit')

# same hidden field stuff as edit group form
# this form is used to delete a user
class DeleteUserForm(FlaskForm):
    email = HiddenField('Email', validators=[DataRequired()])
    delete_user = SubmitField('Delete User')