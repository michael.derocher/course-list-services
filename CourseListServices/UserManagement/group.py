from flask import flash
from flask_login import current_user

class Group:
    def __init__(self, groupname, permissions):
        if not isinstance(groupname, str):
            raise TypeError()
        if (permissions != None) and not isinstance(permissions, str):
            raise TypeError()
        self.groupname = groupname
        
        if permissions == None:
            permissions = ""
        self.permissions = permissions

    # loops through the permissions string field and checks if the permission is in it
    def has_permission(self, permission):
        if not isinstance(permission, str):
            raise TypeError()
        return permission in self.permissions
    
    # returns a list of permissions
    def display_permissions(self):
        permissions = (self.permissions.split(";") if self.permissions else ["None"])
        if isinstance(permissions, list) and permissions[(len(permissions) - 1)] == "":
            permissions.pop()
            
        return permissions
    
    # this adds a permission to the permissions string field
    # this wasn't used or fully implemented
    def add_permission(self, permission):
        if not isinstance(permission, str):
            raise TypeError()
        if self.has_permission(permission):
            return
        if self.permissions:
            self.permissions += ";"
        self.permissions += permission

    # this removes a permission from the permissions string field
    # this wasn't used or fully implemented
    def remove_permission(self, permission):
        if not isinstance(permission, str):
            raise TypeError()
        if not self.has_permission(permission):
            return
        if self.permissions == permission:
            self.permissions = ""
        else:
            self.permissions = self.permissions.replace(f"{permission};", "")
            self.permissions = self.permissions.replace(f";{permission}", "")
            self.permissions = self.permissions.replace(permission, "")
            
    # this returns the permission level of the group
    # this is used to determine the hierarchy between groups and determine
    # if a user can modify another user or stuff like that
    def get_permission_level(self):
        if self.groupname == "root":
            return 4
        elif self.groupname == "admin_gp":
            return 3
        elif self.groupname == "admin_user_gp":
            return 2
        elif self.groupname == "member":
            return 1
        return 0
    
    # this is used to determine if a user can modify another user
    def can_modify_other_user(self, group):
        if not isinstance(group, Group):
            raise TypeError()
        
        user1_perm_value = self.get_permission_level()
        user2_perm_value = group.get_permission_level()
        
        # I tried other things, but they were all finicky.
        # As such, back to the basics
        if current_user.is_anonymous:
            return False
        if group.groupname == "root":
            return False
        if current_user.group.has_permission("canSelfModify") and user1_perm_value >= user2_perm_value:
            return True
        if user1_perm_value > user2_perm_value:
            return True
        return False

    def __repr__(self):
        return f"Group({self.groupname}, {self.permissions})"
    
    def __str__(self):
        if current_user.is_authenticated and current_user.group.has_permission("canSeeGroup"): #and self.can_modify_other_user(current_user.group):
            return f"{self.groupname.capitalize()}"
        return ""