from flask import Blueprint, flash, render_template, request, redirect, url_for, current_app, send_from_directory
from flask_login import login_required, current_user
from werkzeug.security import check_password_hash, generate_password_hash
import flask_login
from ..DB.dbmanager import get_db
from .member import DeleteUserForm, EditGroupForm, User, ResetPasswordForm, EditProfileForm
import os

bp = Blueprint("member", __name__, url_prefix='/members/')

#Gets all members
@bp.route('/', methods=['GET', 'POST'])
@login_required
def get_members():
    try:
        database = get_db()
    except Exception as e:
        flash('Unable to connect with the database')
    #should I switch it all so its members instead of users?
    members=database.get_users()
    
    # sort members by id
    members=sorted(members, key=lambda x: x.id)

    # this is to add the option to delete or modify a user in the profile page
    form = EditGroupForm()
    form2 = DeleteUserForm()
    form.groupname.choices = [(g.groupname) for g in database.get_groups() if g.groupname != 'root' and current_user.group.can_modify_other_user(g)]
    
    if form.validate_on_submit():
        group = database.get_group(form.groupname.data)
        email = form.email.data

        if group:
            database.add_group_member(group.groupname, email)
            return redirect(url_for('member.get_members', members=members, group=group, form=form, form2=form2, user=current_user))
        return render_template('members.html', members=members, group=group, form=form, form2=form2, user=current_user)
    if form2.validate_on_submit():
        email = form2.email.data
        database.delete_user(email)
        return redirect(url_for('member.get_members', members=members, form=form, form2=form2, user=current_user))
    return render_template('members.html', members=members, form=form, form2=form2, user=current_user)

#Gets a single member using their email
@bp.route('/<string:email>')
@login_required
def get_member(email):
    try:
        database = get_db()
    except Exception as e:
        flash('Unable to connect with the database')
    
    member = database.get_user(email)
    if member:
        return render_template('member.html', member=member)
    flash('Member not found!')
    return redirect(url_for('member.get_members'))

#Getting a member's profile page using their email
@bp.route('/profile/<string:email>', methods=['GET', 'POST'])
@login_required
def get_profile(email):
    try:
        member = get_db().get_user(email)
    except Exception as e:
        flash('Unable to connect with the database')
    return render_template('profile.html', member=member)

#Gets the page to reset password
@bp.route('/profile/<string:email>/reset-password/', methods=['GET', 'POST'])
@login_required
def reset_password(email):
    try:
        member = get_db().get_user(email)
    except Exception as e:
        flash('Unable to connect with the database')
    form = ResetPasswordForm()
    #Reset password
    if request.method == 'POST':
        if form.validate_on_submit:
            #Check password first
            pwd = form.old_password.data
            if check_password_hash(member.password, pwd):
                hash = generate_password_hash(form.new_password.data)
                member.password = hash
                try:
                    get_db().update_password(member.id,hash)
                    flash('Password successfully reset')
                except Exception as e:
                    flash(f'Issue in db, {e}')
            else:
                flash("Invalid password")
    return render_template('reset_password.html', member=member, form=form)

#To allow editting of name and avatar for a member
@bp.route('/<string:email>/edit-profile/', methods=['GET', 'POST'])
@login_required
def edit_profile(email):
    form = EditProfileForm()
    try:
        user = get_db().get_user(email)
    except Exception(e):
        flash("error in database!")
        return redirect(url_for('courseComp.display_courses'))
    
    if form.validate_on_submit():
        #Changing member's name
        user.name = form.name.data
        try:
            get_db().update_name(user.id, form.name.data)
        except Exception as e:
            flash('Unable to connect with the database')

        #Changing member's avatar
        if form.avatar.data:
            file = form.avatar.data
            avatar_dir = os.path.join(current_app.config['IMAGE_PATH'], email)
            if not os.path.exists(avatar_dir):
                os.makedirs(avatar_dir)
            avatar_path = os.path.join(avatar_dir, 'avatar.png')
            file.save(avatar_path)

        if form.password.data:
            hash = generate_password_hash(form.password.data)
            user.password = hash
            try:
                get_db().update_password(user.id,hash)
                flash('Password successfully reset')
            except Exception as e:
                flash(f'Issue in db, {e}')

        flash('Your changes have been saved.')
        return redirect(url_for('member.edit_profile', email=email))
    elif request.method == 'GET':
        #Makes it so the name field in form already has the currently used name present in it
        form.name.data = user.name
    return render_template('edit_profile.html', title='Edit Profile', form=form, member=user)
