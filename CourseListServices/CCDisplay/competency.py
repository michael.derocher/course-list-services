class Competency:
    def __init__(self, competency_id, competency, competency_achievement, competency_type):
        if not isinstance(competency_id, str) or competency_id == None:
            raise TypeError("Competency id must be a string")
        if not isinstance(competency, str) or competency == None:
            raise TypeError("Competency must be a string")
        if not isinstance(competency_achievement, str) or competency_achievement == None:
            raise TypeError("Competency achievement must be a string")
        if not isinstance(competency_type, str) or competency_type == None:
            raise TypeError("Competency type must be a string")

        self.competency_id = competency_id
        self.competency = competency
        self.competency_achievement = competency_achievement
        self.competency_type = competency_type


    def __repr__(self):
        return f'Competency({self.competency_id}, {self.competency}, {self.competency_achievement}, {self.competency_type})'
    
    def __str__(self):
        return (f"<div class='competency' id='{ self.competency_id }'> <div class='competency_header'>" +
        f"<div class='comp_id'><a href='{ self.competency_id }'> { self.competency_id }</a></div>" +
        f"<div class='comp_name'> { self.competency } </div> <div class='comp_type'> { self.competency_type }" +
        f"</div> </div> <div class='comp_achievement'>{ self.competency_achievement }</div> </div>")

    def to_json(self):
        return self.__dict__


    @staticmethod
    def from_json(competency_json):
        if not isinstance(competency_json):
            raise TypeError("Invalid type")
        return Competency(competency_json["competency_id"], competency_json["competency"], competency_json["competency_achievement"], competency_json["competency_type"])


from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired, Length
class CompetencyForm(FlaskForm):
    competency_id = StringField('competency_id', validators=[DataRequired(),Length(4,4,"Id must be 4 characters long")])
    competency = StringField('competency', validators=[DataRequired()])
    competency_achievement = TextAreaField('competency_achievement', validators=[DataRequired()])
    competency_type = StringField('competency_type', validators=[DataRequired()])
