class Domain:
    def __init__(self, domain_id, domain, domain_description):
        if not isinstance(domain_id, int) and not None:
            raise TypeError("Domain id must be an integer")
        if not isinstance(domain, str) and not None:
            raise TypeError("Domain must be a string")
        if not isinstance(domain_description, str) and not None:
            raise TypeError("Domain description must be a string")

        self.domain_id = domain_id
        self.domain = domain
        self.domain_description = domain_description

    def __repr__(self):
        return f'Domain({self.domain_id}, {self.domain}, {self.domain_description})'
    
    def __str__(self):
        return (f"<div class='domain' id={self.domain_id}> <div class='domain_header'>" + 
        f"<a href='{self.domain_id}'>{ self.domain_id }. { self.domain }</a> </div> <div class='domain_description'>" +
        f"{ self.domain_description }</div> </div>")
    
    def to_json(self):
        return self.__dict__
    
    @staticmethod
    def from_json(domain_json):
        if not isinstance(domain_json):
            raise TypeError("Invalid type")
        return Domain(
            domain_json["domain_id"], 
            domain_json["domain"], 
            domain_json["domain_description"])
        
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, IntegerField
from wtforms.validators import DataRequired, NumberRange
class DomainForm(FlaskForm):
    domain_id = IntegerField('domain_id', validators=[DataRequired(), NumberRange(0)])
    domain = StringField('domain', validators=[DataRequired()])
    domain_description = TextAreaField('domain_description', validators=[DataRequired()]) 