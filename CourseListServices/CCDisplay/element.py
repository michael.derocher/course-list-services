class Element:
    def __init__(self, element_id, element_order, element, element_criteria, competency_id):
        # might want to make id auto generate instead
        if not isinstance(element_id, int) or element_id == None:
            raise TypeError("Element id must be an integer")
        if not isinstance(element_order, int) or element_order == None:
            raise TypeError("Element order must be an integer")
        if not isinstance(element, str) or element == None:
            raise TypeError("Element must be a string")
        if not isinstance(element_criteria, str) or element_criteria == None:
            raise TypeError("Element criteria must be a string")
        if not isinstance(competency_id, str) or competency_id == None:
            raise TypeError("Competency id must be a string")
        
        self.element_id = element_id
        self.element_order = element_order
        self.element = element
        self.element_criteria = element_criteria
        self.competency_id = competency_id
        
    def set_elem_hours(self, hours):
        self.hours = hours


    def __repr__(self):
        return f'Element({self.element_id}, {self.element_order}, {self.element}, {self.element_criteria}, {self.competency_id})'
    
    def __str__(self):
        return f'{self.element_order}. {self.element} ({self.competency_id}): {self.element_criteria}'

    def to_json(self):
        return self.__dict__


    @staticmethod
    def from_json(competency_json):
        if not isinstance(competency_json):
            raise TypeError("Invalid type")
        return Element(competency_json["element_id"],
                        competency_json["element_order"],
                        competency_json["element"],
                        competency_json["element_criteria"],
                        competency_json["competency_id"])
    
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, IntegerField, SelectField, FloatField
from wtforms.validators import DataRequired, NumberRange
class ElementForm(FlaskForm):
    element_order = IntegerField('element_order', validators=[DataRequired(), NumberRange(1)])
    element = StringField('element', validators=[DataRequired()])
    element_criteria = TextAreaField('element_criteria', validators=[DataRequired()])

class CourseCompetencyForm(FlaskForm):
    competency_id = SelectField('competency', coerce=str, validators=[DataRequired()])

class CourseElementForm(FlaskForm):
    element_id = SelectField('element', coerce=int, validators=[DataRequired()])
    hours = FloatField('hours', validators=[DataRequired(), NumberRange(0)])
    
class CourseElementHoursForm(FlaskForm):
    hours = FloatField('hours', validators=[DataRequired(), NumberRange(0)])
