from flask import Blueprint, render_template, request, redirect, url_for, flash
from flask_login import login_required
from ..DB.dbmanager import get_db
from .course import Course, CourseForm
from .competency import Competency, CompetencyForm
from .element import Element, ElementForm, CourseCompetencyForm, CourseElementForm, CourseElementHoursForm
from .domain import Domain, DomainForm
from oracledb.exceptions import IntegrityError
from .search import SearchForm

bp = Blueprint("courseComp", __name__, url_prefix='/')


@bp.route('/courses/', methods=['get', 'post'])
@bp.route('/', methods=['get', 'post'])
def display_courses():
    courses = get_db().get_courses()
    form = SearchForm(request.form)
    
    domains = get_db().get_domains()
    domain_list = [(domain.domain_id, domain.domain) for domain in domains]
    domain_list.insert(0, ("--Domain--", "--Domain--"))
    form.domain_select.choices = domain_list

    if request.method == 'POST':
        results = []
        if form.validate_on_submit():
            search = form.searchbar.data.lower()
            for course in courses:
                if search in course.course_id.lower() or search in course.course_name.lower() or search in course.course_description.lower() or search in course.domain.domain.lower():
                    if form.term_select.data == "--Term--" and form.domain_select.data == "--Domain--":
                        results.append(course)
                    elif form.term_select.data == "--Term--" and int(form.domain_select.data) == int(course.domain.domain_id):
                        results.append(course)
                    elif form.term_select.data != "--Term--":
                        if int(form.term_select.data) == int(course.term.term_id) and form.domain_select.data == "--Domain--":
                            results.append(course)
                        elif int(form.term_select.data) == int(course.term.term_id) and int(form.domain_select.data) == int(course.domain.domain_id):
                            results.append(course)
                elif search == "search":
                    if form.term_select.data == "--Term--" and form.domain_select.data == "--Domain--":
                        results.append(course)
                    elif form.term_select.data == "--Term--" and int(form.domain_select.data) == int(course.domain.domain_id):
                        results.append(course)
                    elif form.term_select.data != "--Term--":
                        if int(form.term_select.data) == int(course.term.term_id) and form.domain_select.data == "--Domain--":
                            results.append(course)
                        elif int(form.term_select.data) == int(course.term.term_id) and int(form.domain_select.data) == int(course.domain.domain_id):
                            results.append(course)

            if not results:
                flash('no result found!')
                return redirect(url_for('courseComp.display_courses'))
            return render_template('course_results.html', results=results, form=form)
        else:
            flash('please fill out at least one of the fields')

    return render_template('courses.html', courses=courses, form=form)

@bp.route('/courses/<string:course_id>/')
def specific_course(course_id):
    course = get_db().get_specific_course(course_id)
    course_competencies = get_db().get_course_competencies(course_id)
    course_comp_elements = get_db().get_course_elements(course_id)
    return render_template('specific_course.html', course=course, competencies=course_competencies, elements=course_comp_elements)

@bp.route('/courses/add', methods=['GET', 'POST'])
@login_required
def add_course():
    form = CourseForm()
    domains = get_db().get_domains()
    domain_list = [(domain.domain_id, domain.domain) for domain in domains]
    form.domain.choices = domain_list
    terms = get_db().get_terms()
    term_list = [(term.term_id) for term in terms]
    form.term.choices = term_list
    if request.method == 'POST':
        if form.validate_on_submit():
            newCourse = Course(form.course_id.data,
                                   form.course_name.data,
                                   form.theory_hours.data,
                                   form.lab_hours.data,
                                   form.work_hours.data,
                                   0,
                                   float(0),
                                   form.course_description.data,
                                   get_db().get_domain(form.domain.data),
                                   get_db().get_term(form.term.data))
            try:
                get_db().add_course(newCourse)
                return redirect(url_for('courseComp.specific_course', course_id=newCourse.course_id))
            except IntegrityError:
                flash("A course with that id already exists")
        else:
            flash('Invalid input')
    return render_template('add_course.html', form=form)

@bp.route('/courses/<string:course_id>/edit/', methods=['GET', 'POST'])
@login_required
def edit_course(course_id):
    course = get_db().get_specific_course(course_id)
    form = CourseForm()
    form.course_id.data = course_id
    domains = get_db().get_domains()
    domain_list = [(domain.domain_id, domain.domain) for domain in domains]
    form.domain.choices = domain_list
    terms = get_db().get_terms()
    term_list = [(term.term_id) for term in terms]
    form.term.choices = term_list
    if request.method == 'POST':
        if form.validate_on_submit():
            updatedCourse = Course(course_id,
                                   form.course_name.data,
                                   form.theory_hours.data,
                                   form.lab_hours.data,
                                   form.work_hours.data,
                                   get_db().get_course_hours(course_id),
                                   float(get_db().get_course_credits(course_id)),
                                   form.course_description.data,
                                   get_db().get_domain(form.domain.data),
                                   get_db().get_term(form.term.data))
            get_db().edit_course(updatedCourse)
            return redirect(url_for('courseComp.specific_course', course_id=course_id))
        else:
            flash('Invalid input')
    elif request.method == 'GET':
        form.course_name.data = course.course_name
        form.theory_hours.data = course.theory_hours
        form.lab_hours.data = course.lab_hours
        form.work_hours.data = course.work_hours
        form.course_description.data = course.course_description
        form.domain.data = course.domain.domain_id
        form.term.data = course.term.term_id
    return render_template('edit_course.html', form=form, course=course)

@bp.route('/courses/<string:course_id>/delete/')
@login_required
def delete_course(course_id):
    get_db().delete_course(course_id)
    flash("Course deleted")
    return redirect(url_for('courseComp.display_courses'))

@bp.route('/courses/<string:course_id>/add-element-hours/', methods=['GET', 'POST'])
@login_required
def add_course_competency(course_id):
    form = CourseCompetencyForm()
    competencies = get_db().get_competencies()
    competency_list = [(comp.competency_id, (comp.competency_id + ' ' + comp.competency)) for comp in competencies]
    form.competency_id.choices = competency_list
    if request.method == 'POST':
        if form.validate_on_submit():
            return redirect(url_for('courseComp.add_course_competency_element', course_id=course_id, competency_id=form.competency_id.data))
        else:
            flash('Invalid input')
    return render_template('add_course_competency.html', form=form)

@bp.route('/courses/<string:course_id>/add-element-hours/<string:competency_id>/', methods=['GET', 'POST'])
@login_required
def add_course_competency_element(course_id, competency_id):
    form = CourseElementForm()
    elements = get_db().get_competency_elements(competency_id)
    element_list = [(elem.element_id, elem.element) for elem in elements]
    form.element_id.choices = element_list
    if request.method == 'POST':
        if form.validate_on_submit():
            get_db().add_course_element(course_id, form.element_id.data, form.hours.data)
            return redirect(url_for('courseComp.specific_course', course_id=course_id))
        else:
            flash('Invalid input')
    return render_template('add_course_competency_element.html', form=form)

@bp.route('/courses/<string:course_id>/edit-element-hours/<string:element_id>/', methods=['GET', 'POST'])
@login_required
def edit_course_competency_element(course_id, element_id):
    form = CourseElementHoursForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            get_db().edit_course_element(course_id, element_id, form.hours.data)
            return redirect(url_for('courseComp.specific_course', course_id=course_id))
        else:
            flash('Invalid input')
    return render_template('edit_course_competency_element.html', form=form)
    

@bp.route('/courses/<string:course_id>/delete-element-hours/<string:element_id>/')
@login_required
def delete_course_competency_element(course_id, element_id):
    get_db().delete_course_element(course_id, element_id)
    flash("Course Element Deleted")
    return redirect(url_for('courseComp.specific_course', course_id=course_id))


@bp.route('/competencies/', methods=['get', 'post'])
def display_competencies():
    competencies = get_db().get_competencies()
    form = SearchForm(request.form)

    if request.method == 'POST':
        results = []
        search = form.searchbar.data.lower()
        for competency in competencies:
            if search in competency.competency.lower() or search in competency.competency_achievement.lower() or search in competency.competency_type.lower() or search in competency.competency_id.lower():
                results.append(competency)

        if not results:
            flash('no result found!')
            return redirect(url_for('courseComp.display_competencies'))
        return render_template('results.html', results=results, form=form, header="COMPETENCIES")

    return render_template('competencies.html', competencies=competencies, form=form)

@bp.route('/competencies/<string:competency_id>/')
def specific_competency(competency_id):
    competency = get_db().get_specific_competency(competency_id)
    elements = get_db().get_competency_elements(competency_id)
    relevant_courses = get_db().get_relevant_courses(competency_id)
    return render_template('specific_competency.html', competency=competency, elements=elements, relevant_courses=relevant_courses)

@bp.route('/competencies/add/', methods=['GET', 'POST'])
@login_required
def add_competency():
    form = CompetencyForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            newComp = Competency(form.competency_id.data,
                                form.competency.data,
                                form.competency_achievement.data,
                                form.competency_type.data)
            try:
                get_db().add_competency(newComp)
                return redirect(url_for('courseComp.specific_competency', competency_id=newComp.competency_id))
            except IntegrityError:
                flash("A competency with that id already exists")
        else:
            flash('Invalid input')
    return render_template('add_competency.html', form=form)

@bp.route('/competencies/<string:competency_id>/edit/', methods=['GET', 'POST'])
@login_required
def edit_competency(competency_id):
    competency = get_db().get_specific_competency(competency_id)
    form = CompetencyForm()
    form.competency_id.data = competency.competency_id
    if request.method == 'POST':
        if form.validate_on_submit():
            updatedComp = Competency(competency_id,
                                     form.competency.data,
                                     form.competency_achievement.data,
                                     form.competency_type.data)
            get_db().edit_competency(updatedComp)
            return redirect(url_for('courseComp.specific_competency', competency_id=competency_id))
        else:
            flash('Invalid input')
    elif request.method == 'GET':
        form.competency.data = competency.competency
        form.competency_achievement.data = competency.competency_achievement
        form.competency_type.data = competency.competency_type
    return render_template('edit_competency.html', form=form, competency=competency)

@bp.route('/competencies/<string:competency_id>/delete/')
@login_required
def delete_competency(competency_id):
    get_db().delete_competency(competency_id)
    flash("Competency deleted")
    return redirect(url_for('courseComp.display_competencies'))

@bp.route('/competencies/<string:competency_id>/add-element/', methods=['GET', 'POST'])
@login_required
def add_competency_element(competency_id):
    form = ElementForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            newElem = Element(0,
                            form.element_order.data,
                            form.element.data,
                            form.element_criteria.data,
                            competency_id)
            get_db().add_element(newElem)
            return redirect(url_for('courseComp.specific_competency', competency_id=competency_id))
        else:
            flash('Invalid input')
    return render_template('add_edit_element.html', form=form)

@bp.route('/competencies/<string:competency_id>/<string:element_id>/edit/', methods=['GET', 'POST'])
@login_required
def edit_competency_element(competency_id, element_id):
    element = get_db().get_element(element_id)
    form = ElementForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            updatedElement = Element(int(element_id),
                                     form.element_order.data,
                                     form.element.data,
                                     form.element_criteria.data,
                                     competency_id)
            get_db().edit_element(updatedElement)
            return redirect(url_for('courseComp.specific_competency', competency_id=competency_id))
        else:
            flash('Invalid input')
    elif request.method == 'GET':
        form.element_order.data = element.element_order
        form.element.data = element.element
        form.element_criteria.data = element.element_criteria
    return render_template('add_edit_element.html', form=form, element=element)

@bp.route('/competencies/<string:competency_id>/<string:element_id>/delete/')
@login_required
def delete_competency_element(competency_id, element_id):
    get_db().delete_element(element_id)
    flash("Element deleted")
    return redirect(url_for('courseComp.specific_competency', competency_id=competency_id))

@bp.route('/domains/', methods=['get', 'post'])
def display_domains():
    domains = get_db().get_domains()
    form = SearchForm(request.form)

    if request.method == 'POST':
        results = []
        search = form.searchbar.data.lower()
        for domain in domains:
            if search in domain.domain.lower() or search in domain.domain_description.lower():
                results.append(domain)
            elif search.isnumeric():
                if int(search) == domain.domain_id:
                    results.append(domain)
        
        if not results:
            flash('no result found!')
            return redirect(url_for('courseComp.display_domains'))
        return render_template('results.html', results=results, form=form, header="DOMAINS")
    
    return render_template('domains.html', domains=domains, form=form)


@bp.route('/domains/<string:domain_id>/')
def specific_domain(domain_id):
    domain = get_db().get_specific_domain(domain_id)
    relevant_courses = get_db().get_domain_courses(domain_id)
    return render_template('specific_domain.html', domain=domain, relevant_courses=relevant_courses)

@bp.route('/domains/add/', methods=['GET', 'POST'])
@login_required
def add_domain():
    form = DomainForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            newDomain = Domain(form.domain_id.data,
                               form.domain.data,
                               form.domain_description.data)
            try:
                get_db().add_domain(newDomain)
                return redirect(url_for('courseComp.specific_domain', domain_id=newDomain.domain_id))
            except IntegrityError:
                flash("A domain with that id already exists")
        else:
            flash('Invalid input')
    return render_template('add_domain.html', form=form)

@bp.route('/domains/<string:domain_id>/edit/', methods=['GET', 'POST'])
@login_required
def edit_domain(domain_id):
    domain = get_db().get_specific_domain(domain_id)
    form = DomainForm()
    form.domain_id.data = domain.domain_id
    if request.method == 'POST':
        if form.validate_on_submit():
            updatedDomain = Domain(domain.domain_id,
                                   form.domain.data,
                                   form.domain_description.data)
            get_db().edit_domain(updatedDomain)
            return redirect(url_for('courseComp.specific_domain', domain_id=domain_id))
        else:
            flash('Invalid input')
    elif request.method == 'GET':
        form.domain.data = domain.domain
        form.domain_description.data = domain.domain_description
    return render_template('edit_domain.html', form=form, domain=domain)

@bp.route('/domains/<string:domain_id>/delete/')
@login_required
def delete_domain(domain_id):
    get_db().delete_domain(domain_id)
    flash("Domain deleted")
    return redirect(url_for('courseComp.display_domains'))