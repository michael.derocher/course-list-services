from CourseListServices.CCDisplay.domain import Domain
from CourseListServices.CCDisplay.term import Term

class Course:
    def __init__(self, course_id, course_name, theory_hours, lab_hours, work_hours, course_hours, course_credits, course_description, domain, term):
        if not isinstance(course_id, str) or course_id == None:
            raise TypeError("Course id must be a string")
        if not isinstance(course_name, str) or course_name == None:
            raise TypeError("Course name must be a string")
        if not isinstance(theory_hours, int) or theory_hours == None:
            raise TypeError("Theory hours must be an integer")
        if not isinstance(lab_hours, int) or lab_hours == None:
            raise TypeError("Lab hours must be an integer")
        if not isinstance(work_hours, int) or work_hours == None:
            raise TypeError("Work hours must be an integer")
        if not isinstance(course_hours, int) or course_hours == None:
            raise TypeError("Course hours must be an integer")
        if not isinstance(course_credits, float) or course_credits == None:
            raise TypeError("Course credits must be a float type")
        if not isinstance(course_description, str) or course_description == None:
            raise TypeError("Course description must be a string")
        if not isinstance(domain, Domain) or domain == None:
            raise TypeError("Domain must be a Domain")
        if not isinstance(term, Term) or term == None:
            raise TypeError("Term must be an Term")

        self.course_id = course_id
        self.course_name = course_name
        self.theory_hours = theory_hours
        self.lab_hours = lab_hours
        self.work_hours = work_hours
        self.course_hours = course_hours
        self.course_credits = round(course_credits,2)
        self.course_description = course_description
        self.domain = domain
        self.term = term
        
    def __repr__(self):
        return f'Course({self.course_id}, {self.course_name}, {self.theory_hours}, ' \
            f'{self.lab_hours}, {self.work_hours}, {self.course_hours}, {self.course_credits}, {self.course_description}, {self.domain.domain}, {self.term.term_name})'
    
    #the links in here don't work for some reason but keeping this for now just in case
    def __str__(self):
        return (f"<div class='course' id='{ self.course_id }'> <div class='course_header'> <div class='course_id'>" +
        f"<a href='{ self.course_id }'> { self.course_id } </a></div> <div class='course_name'> { self.course_name }" +
        f"</div> <div class='course_clh'> { self.theory_hours }-{ self.lab_hours }-{ self.work_hours }</div>" +
        f"<div class='course_hrs'>{self.course_hours}</div> <div class='course_term'>Term { self.term.term_id }" +
        f"</div> <div class='course_domain'><a href='{ self.domain.domain_id }'>{{ self.domain.domain }}</a></div>" +
        f"</div> <div class='course_description'>{ self.course_description }</div> </div>")

    def to_json(self):
        return {
                "course_id":self.course_id,
                "course_name":self.course_name,
                "theory_hours":self.theory_hours,
                "lab_hours":self.lab_hours,
                "work_hours":self.work_hours,
                "course_hours":self.course_hours,
                "course_credits":self.course_credits,
                "course_description":self.course_description,
                "domain":self.domain.to_json(),
                "term":self.term.to_json()
                }

    @staticmethod
    def from_json(course_json):
         if not isinstance(course_json):
            raise TypeError("Invalid type")
         return Course(
                        course_json["course_id"],
                        course_json["course_name"],
                        course_json["theory_hours"],
                        course_json["lab_hours"],
                        course_json["work_hours"],
                        course_json["course_hours"],
                        course_json["course_credits"],
                        course_json["course_description"],
                        Domain.from_json(course_json["domain"]),
                        Term.from_json(course_json["term"])
                        )
    
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, IntegerField, SelectField
from wtforms.validators import DataRequired, Length, NumberRange
class CourseForm(FlaskForm):
    course_id = StringField('course_id', validators=[DataRequired(),Length(9,10,"Id must be 4 characters long")])
    course_name = StringField('course_name', validators=[DataRequired()])
    theory_hours = IntegerField('theory_hours', validators=[DataRequired(), NumberRange(0)])
    lab_hours = IntegerField('lab_hours', validators=[DataRequired(), NumberRange(0)])
    work_hours = IntegerField('work_hours', validators=[DataRequired(), NumberRange(0)])
    course_description = TextAreaField('course_description', validators=[DataRequired()])
    domain = SelectField('domain', coerce=int, validators=[DataRequired()])
    term = SelectField('term', coerce=int, validators=[DataRequired()])