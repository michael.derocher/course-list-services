from flask_wtf import FlaskForm
from wtforms import SubmitField, SearchField, SelectField
from wtforms.validators import DataRequired
class SearchForm(FlaskForm):
    #maybe add other validators?
    searchbar = SearchField("Search", validators=[], default="Search")
    term_select = SelectField('Term',choices=[('--Term--', '--Term--'),(1,1),(2,2),(3,3),(4,4),(5,5),(6,6)])
    domain_select = SelectField('Domain', choices=[])

    def validate(self, extra_validators=None):
        if super().validate(extra_validators):
            if self.searchbar.data == "Search" and self.term_select.data == "--Term--" and self.domain_select.data == "--Domain--":
                self.searchbar.errors.append('At least one field must be filled out')
                return False
            else:
                return True

    submit = SubmitField("Submit")
