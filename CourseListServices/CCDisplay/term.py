class Term:
    def __init__(self, term_id, term_name):
        if not isinstance(term_id, int) and not None:
            raise TypeError("Term id must be an integer")
        if not isinstance(term_name, str) and not None:
            raise TypeError("Term name must be a string")

        self.term_id = term_id
        self.term_name = term_name

    def __repr__(self):
        return f'Term({self.term_id}, {self.term_name})'
    
    def __str__(self):
        return f'{self.term_id} {self.term_name}'
    
    def to_json(self):
        return self.__dict__
    
    @staticmethod
    def from_json(term_json):
        if not isinstance(term_json):
            raise TypeError("Invalid type")
        return Term(term_json["term_id"], term_json["term_name"])