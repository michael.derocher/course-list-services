from flask import Blueprint, jsonify, request, make_response, url_for
from oracledb.exceptions import IntegrityError
from ..DB.dbmanager import get_db
from .course import Course
from .domain import Domain
from .term import Term
from .element import Element
from .competency import Competency

bp = Blueprint('course_comp_api', __name__, url_prefix='/api/courses/')

@bp.route('/', methods=['GET','POST', 'PUT', 'DELETE'])
def course_api():
    if request.method == 'POST':
        if request.json:
            content = request.json
            newCourse = Course(content['course_id'],
                                content['course_name'],
                                content['theory_hours'],
                                content['lab_hours'],
                                content['work_hours'],
                                0,
                                float(0),
                                content['course_description'],
                                Domain(content['domain_id'],
                                      content['domain'],
                                      content['domain_description']),
                                Term(content['term_id'],
                                     content['term_name'])
                                )
            try:
                get_db().add_course(newCourse)
                resp = make_response({}, 200)
                resp.headers["location"] = url_for("course_comp_api.course_api", course_id=newCourse.course_id)
                return resp
            
            except IntegrityError:
                return make_response({
                    "id":400,
                    "description":"A course with this Id already exists"
                }, 400)
        else:
            return make_response({
                    "id":400,
                    "description":"Json parameter not found in request"
                }, 400)
    elif request.method == 'PUT':
        if request.json:
            content = request.json
            updatedCourse = Course(content['course_id'],
                                content['course_name'],
                                content['theory_hours'],
                                content['lab_hours'],
                                content['work_hours'],
                                0,
                                float(0),
                                content['course_description'],
                                Domain(content['domain_id'],
                                      content['domain'],
                                      content['domain_description']),
                                Term(content['term_id'],
                                     content['term_name'])
                                )
            get_db().edit_course(updatedCourse)
            resp = make_response({}, 201)
            resp.headers["location"] = url_for("course_comp_api.course_api", course_id=updatedCourse.course_id)
            return resp
        else:
            return make_response({
                    "id":400,
                    "description":"Json parameter not found in request"
                }, 400)
    elif request.method == 'DELETE':
        if request.args.get('course_id'):
            get_db().delete_course(request.args.get('course_id'))
            return make_response({}, 200)
        else:
            return make_response({
                "id":400,
                "description":"Parameter course_id not found"
            }, 400)
    elif request.method == 'GET':
        if request.args:
            course_id = request.args.get('course_id')
            courses = get_db().get_courses()
            course = [course for course in courses if course.course_id == course_id]
            return make_response(jsonify(course[0].to_json()), 200)
        
    courses = get_db().get_courses()
    json = [course.to_json() for course in courses]
    return jsonify(json)


@bp.route('/<string:course_id>/elements/', methods=['GET','POST', 'PUT', 'DELETE'])
def element_api(course_id):
    if request.method == 'POST':
        if request.json:
            content = request.json
            newElement = Element(0,
                                 content["element_order"],
                                 content["element"],
                                 content["element_criteria"],
                                 content["competency_id"])
            get_db().add_element(newElement)
            resp = make_response({}, 200)
            resp.headers["location"] = url_for("course_comp_api.element_api", course_id=course_id)
            return resp
        else:
            return make_response({
                    "id":400,
                    "description":"Json parameter not found in request"
                }, 400)
    elif request.method == 'PUT':
        if request.json:
            content = request.json
            updatedElement = Element(content["element_id"],
                                 content["element_order"],
                                 content["element"],
                                 content["element_criteria"],
                                 content["competency_id"])
            get_db().edit_element(updatedElement)
            resp = make_response({}, 201)
            resp.headers["location"] = url_for("course_comp_api.element_api", course_id=course_id, element_id=updatedElement.element_id)
            return resp
        else:
            return make_response({
                    "id":400,
                    "description":"Json parameter not found in request"
                }, 400)
    elif request.method == 'DELETE':
        if request.args.get('element_id'):
            get_db().delete_element(int(request.args.get('element_id')))
            return make_response({}, 200)
        else:
            return make_response({
                "id":400,
                "description":"Parameter element_id not found"
            }, 400)
    elif request.method == 'GET':
        if request.args:
            element_id = int(request.args.get('element_id'))
            elements = get_db().get_course_elements(course_id)
            element = [element for element in elements if element.element_id == element_id]
            return make_response(jsonify(element[0].to_json()), 200)
        
    elements = get_db().get_course_elements(course_id)
    json = [element.to_json() for element in elements]
    return jsonify(json)


@bp.route('/<string:course_id>/elements/competencies/', methods=['GET','POST', 'PUT', 'DELETE'])
def competency_api(course_id):
    if request.method == 'POST':
        if request.json:
            content = request.json
            newCompetency = Competency(content["competency_id"],
                                 content["competency"],
                                 content["competency_achievement"],
                                 content["competency_type"])
            get_db().add_competency(newCompetency)
            resp = make_response({}, 200)
            resp.headers["location"] = url_for("course_comp_api.competency_api", course_id=course_id, competency_id=newCompetency.competency_id)
            return resp
        else:
            return make_response({
                    "id":400,
                    "description":"Json parameter not found in request"
                }, 400)
    elif request.method == 'DELETE':
        if request.args.get('competency_id'):
            get_db().delete_competency(request.args.get('competency_id'))
            return make_response({}, 200)
        else:
            return make_response({
                "id":400,
                "description":"Parameter competency_id not found"
            }, 400)
    elif request.method == 'GET':
        if request.args:
            competency_id = request.args.get('competency_id')
            competencies = get_db().get_course_competencies(course_id)
            competency = [competency for competency in competencies if competency.competency_id == competency_id]
            return jsonify(competency[0].to_json())
        
    competencies = get_db().get_course_competencies(course_id)
    json = [competency.to_json() for competency in competencies]
    return jsonify(json)